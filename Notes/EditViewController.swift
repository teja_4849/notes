//
//  EditViewController.swift
//  Notes
//
//  Created by MALLOJJALA PAVAN TEJA on 4/16/20.
//  Copyright © 2020 MALLOJJALAPAVANTEJA. All rights reserved.
//

import UIKit

class EditViewController: UIViewController {

    @IBOutlet weak var noteName: UITextField!
    @IBOutlet weak var notes: UITextView!
    @IBOutlet weak var save: UIButton!
    
    var noteId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Edit Notes"

        // Do any additional setup after loading the view.
        
        setTextFeilds()
        
        displayNotes()
        
        dismissKeyboard()

        
    }
    
    @IBAction func saveNote(_ sender: Any) {
        
        let itemsObject = UserDefaults.standard.object(forKey: "items")
        var items: [String:String]
        if let tempItems = itemsObject as? [String:String] {
            items = tempItems
            items[noteId] = notes.text!
        } else {
            items = [noteId :notes.text!]
        }
        UserDefaults.standard.set(items, forKey: "items")

    }
    
    func setTextFeilds()  {
        notes.layer.borderWidth = 0.4
        notes.layer.borderColor = UIColor.gray.cgColor
        notes.layer.cornerRadius = notes.frame.size.width/19
        
        noteName.layer.borderWidth = 0.4
        noteName.layer.borderColor = UIColor.gray.cgColor
    }
    func displayNotes()  {
        guard let itemsObject = UserDefaults.standard.object(forKey: "items") as? [String:String] else {return}
        let items = itemsObject
        let selectedNote =  items.filter({$0.key==noteId}).map({$0})
        noteName.text = selectedNote.first?.key
        notes.text = selectedNote.first?.value
    }
    
    func dismissKeyboard()  {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(gesture:)))
        self.view.addGestureRecognizer(tap)
        
    }
    @objc func handleTap (gesture: UITapGestureRecognizer) {
        view.endEditing(true)
        
    }
    

}
