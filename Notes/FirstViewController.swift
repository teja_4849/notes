//
//  FirstViewController.swift
//  Notes
//
//  Created by MALLOJJALA PAVAN TEJA on 4/16/20.
//  Copyright © 2020 MALLOJJALAPAVANTEJA. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {
    
    @IBOutlet weak var list: UITableView!
    
    var items:[String:String] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.title = "Notes"
    }


    override func viewDidAppear(_ animated: Bool) {
        guard let itemsObject = UserDefaults.standard.object(forKey: "items") as? [String:String] else {return}
        items = itemsObject
        
        list.reloadData()
    }
    
}

extension FirstViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "listCell"
        guard let cell = list.dequeueReusableCell(withIdentifier: cellIdentifier) else {fatalError("Issue dequeuing \(cellIdentifier)")}
        
        var cellLable = ""
        let itemNames = items.map({$0.key})
        
        let tempLable = itemNames[indexPath.row]
        cellLable = tempLable
        
        cell.textLabel?.text = cellLable
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let editVC = storyBoard.instantiateViewController(withIdentifier: "editVC") as! EditViewController
        let itemNames = items.map({$0.key})
        let currentItem = itemNames[indexPath.row]
        editVC.noteId = currentItem
        self.navigationController?.pushViewController(editVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let itemNames = items.map({$0.key})
            let currentItem = itemNames[indexPath.row]
            if let ind = items.index(forKey: currentItem) {
                items.remove(at: ind)
                
            }
            list.reloadData()
            UserDefaults.standard.set(items, forKey: "items")
            
        }
    }
}
