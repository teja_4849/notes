//
//  SecondViewController.swift
//  Notes
//
//  Created by MALLOJJALA PAVAN TEJA on 4/16/20.
//  Copyright © 2020 MALLOJJALAPAVANTEJA. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var itemtextFeild: UITextField!
    @IBOutlet weak var notesTextView: UITextView!
    @IBOutlet weak var addNote: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = "Add new Notes"
        setTextFeilds()
        
        dismissKeyboard()
    }

    @IBAction func add(_ sender: Any) {
        
        let itemsObject = UserDefaults.standard.object(forKey: "items")
        var items: [String:String]
        if let noteName = itemtextFeild.text, !noteName.isEmpty, let note = notesTextView.text, !note.isEmpty {

        if let tempItems = itemsObject as? [String:String] {
            items = tempItems
            items[noteName] = note
        } else {
            items = [noteName :note]
            }
        UserDefaults.standard.set(items, forKey: "items")
       
        } else  {
            
            if let tempItems = itemsObject as? [String:String] {
                items = tempItems
                items["New Note"] = ""
            } else {
                items = ["New Note" :""]
            }
            UserDefaults.standard.set(items, forKey: "items")
        }
        itemtextFeild.text = ""
        notesTextView.text = ""
        
    }
    
    func setTextFeilds()  {
        notesTextView.layer.borderWidth = 0.4
        notesTextView.layer.borderColor = UIColor.gray.cgColor
        notesTextView.layer.cornerRadius = notesTextView.frame.size.width/19
        
        itemtextFeild.layer.borderWidth = 0.4
        itemtextFeild.layer.borderColor = UIColor.gray.cgColor
    }
    
    func dismissKeyboard()  {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(gesture:)))
        self.view.addGestureRecognizer(tap)
        
    }
    @objc func handleTap (gesture: UITapGestureRecognizer) {
        view.endEditing(true)
        
    }
    
}

